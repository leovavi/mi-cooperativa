﻿using IBM.Data.Informix;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiCooperativa
{
    public partial class Cierre_Mensual : Form
    {
        string userLogged, loggedAs;
        Informix ifx;
        public Cierre_Mensual(string user, string logged)
        {
            InitializeComponent();
            ifx = new Informix();
            userLogged = user;
            loggedAs = logged;
            lbl_User.Text = "Usuario: " + userLogged;
        }

        private void btn_Atras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Administrador admin = new Administrador(userLogged, loggedAs);
            admin.Show();
            admin.Closed += (s, args) => this.Close();
        }

        private void btn_Cierre_Click(object sender, EventArgs e)
        {
            IfxConnection con = ifx.getConnection();
            try
            {
                IfxCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_CIERRE_MENSUAL_CREATE";
                cmd.Parameters.Add("P_ANIO", IfxType.Integer).Value = Int32.Parse(txt_Año.Text);
                cmd.Parameters.Add("P_MES", IfxType.Integer).Value = Int32.Parse(txt_Mes.Text);
                cmd.ExecuteNonQuery();

                string msg = "Cierre Mensual generado";
                MessageBox.Show(this, msg, "Cierre Mensual", MessageBoxButtons.OK);
            }
            catch(Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error al Generar Cierre", MessageBoxButtons.OK);
            }
        }
    }
}
