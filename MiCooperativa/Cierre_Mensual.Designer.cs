﻿namespace MiCooperativa
{
    partial class Cierre_Mensual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Año = new System.Windows.Forms.Label();
            this.lbl_Mes = new System.Windows.Forms.Label();
            this.lbl_Cierre_Mensual = new System.Windows.Forms.Label();
            this.btn_Cierre = new System.Windows.Forms.Button();
            this.btn_Atras = new System.Windows.Forms.Button();
            this.lbl_User = new System.Windows.Forms.Label();
            this.txt_Año = new System.Windows.Forms.MaskedTextBox();
            this.txt_Mes = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // lbl_Año
            // 
            this.lbl_Año.AutoSize = true;
            this.lbl_Año.Location = new System.Drawing.Point(193, 147);
            this.lbl_Año.Name = "lbl_Año";
            this.lbl_Año.Size = new System.Drawing.Size(31, 21);
            this.lbl_Año.TabIndex = 1;
            this.lbl_Año.Text = "Año";
            // 
            // lbl_Mes
            // 
            this.lbl_Mes.AutoSize = true;
            this.lbl_Mes.Location = new System.Drawing.Point(372, 147);
            this.lbl_Mes.Name = "lbl_Mes";
            this.lbl_Mes.Size = new System.Drawing.Size(32, 21);
            this.lbl_Mes.TabIndex = 2;
            this.lbl_Mes.Text = "Mes";
            // 
            // lbl_Cierre_Mensual
            // 
            this.lbl_Cierre_Mensual.AutoSize = true;
            this.lbl_Cierre_Mensual.Font = new System.Drawing.Font("Oswald", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Cierre_Mensual.Location = new System.Drawing.Point(244, 71);
            this.lbl_Cierre_Mensual.Name = "lbl_Cierre_Mensual";
            this.lbl_Cierre_Mensual.Size = new System.Drawing.Size(112, 26);
            this.lbl_Cierre_Mensual.TabIndex = 4;
            this.lbl_Cierre_Mensual.Text = "Cierre Mensual";
            // 
            // btn_Cierre
            // 
            this.btn_Cierre.Location = new System.Drawing.Point(225, 268);
            this.btn_Cierre.Name = "btn_Cierre";
            this.btn_Cierre.Size = new System.Drawing.Size(146, 30);
            this.btn_Cierre.TabIndex = 5;
            this.btn_Cierre.Text = "Realizar Cierre Mensual";
            this.btn_Cierre.UseVisualStyleBackColor = true;
            this.btn_Cierre.Click += new System.EventHandler(this.btn_Cierre_Click);
            // 
            // btn_Atras
            // 
            this.btn_Atras.Location = new System.Drawing.Point(497, 373);
            this.btn_Atras.Name = "btn_Atras";
            this.btn_Atras.Size = new System.Drawing.Size(75, 30);
            this.btn_Atras.TabIndex = 6;
            this.btn_Atras.Text = "Atrás";
            this.btn_Atras.UseVisualStyleBackColor = true;
            this.btn_Atras.Click += new System.EventHandler(this.btn_Atras_Click);
            // 
            // lbl_User
            // 
            this.lbl_User.AutoSize = true;
            this.lbl_User.Location = new System.Drawing.Point(12, 378);
            this.lbl_User.Name = "lbl_User";
            this.lbl_User.Size = new System.Drawing.Size(88, 21);
            this.lbl_User.TabIndex = 7;
            this.lbl_User.Text = "lblUserLogged";
            // 
            // txt_Año
            // 
            this.txt_Año.Location = new System.Drawing.Point(161, 192);
            this.txt_Año.Mask = "9999";
            this.txt_Año.Name = "txt_Año";
            this.txt_Año.Size = new System.Drawing.Size(100, 29);
            this.txt_Año.TabIndex = 8;
            // 
            // txt_Mes
            // 
            this.txt_Mes.Location = new System.Drawing.Point(336, 192);
            this.txt_Mes.Mask = "99";
            this.txt_Mes.Name = "txt_Mes";
            this.txt_Mes.Size = new System.Drawing.Size(100, 29);
            this.txt_Mes.TabIndex = 9;
            // 
            // Cierre_Mensual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 415);
            this.Controls.Add(this.txt_Mes);
            this.Controls.Add(this.txt_Año);
            this.Controls.Add(this.lbl_User);
            this.Controls.Add(this.btn_Atras);
            this.Controls.Add(this.btn_Cierre);
            this.Controls.Add(this.lbl_Cierre_Mensual);
            this.Controls.Add(this.lbl_Mes);
            this.Controls.Add(this.lbl_Año);
            this.Font = new System.Drawing.Font("Oswald", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "Cierre_Mensual";
            this.Text = "Cierre_Mensual";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_Año;
        private System.Windows.Forms.Label lbl_Mes;
        private System.Windows.Forms.Label lbl_Cierre_Mensual;
        private System.Windows.Forms.Button btn_Cierre;
        private System.Windows.Forms.Button btn_Atras;
        private System.Windows.Forms.Label lbl_User;
        private System.Windows.Forms.MaskedTextBox txt_Año;
        private System.Windows.Forms.MaskedTextBox txt_Mes;
    }
}